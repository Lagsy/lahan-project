package com.upnvj.lahan.auth;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.upnvj.lahan.R;


public class AuthActivity extends AppCompatActivity {
    Button BtnLoginAuth,BtnRegisterAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auth);

        BtnLoginAuth = (Button) findViewById(R.id.btn_login_auth);
        BtnRegisterAuth = (Button) findViewById(R.id.btn_register_auth);


        BtnLoginAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AuthActivity.this , LoginActivity.class));
            }
        });
        BtnRegisterAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(AuthActivity.this , RegisterActivity.class));
            }
        });
    }
}