package com.upnvj.lahan.auth;

public class account_users {
    String email;
    String nim;
    String name;
    String jurusan;

    public account_users(String email, String name, String nim, String jurusan) {
        this.email =  email;
        this.name = name;
        this.nim = nim;
        this.jurusan = jurusan;
    }
    // method Setter Getter

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNim() {
        return nim;
    }

    public void setNim(String nim) {
        this.nim = nim;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getJurusan() {
        return jurusan;
    }

    public void setJurusan(String jurusan) {
        this.jurusan = jurusan;
    }
}
