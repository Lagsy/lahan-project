package com.upnvj.lahan.homepage.home.detail;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.os.Bundle;
import android.widget.ImageView;

import com.upnvj.lahan.R;

public class DetailActivity extends AppCompatActivity {
    CardView CvDetail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        CvDetail = (CardView)findViewById(R.id.cv_image_header);
    }
}