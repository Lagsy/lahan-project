package com.upnvj.lahan.homepage.tab.drawer;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.upnvj.lahan.R;
import com.upnvj.lahan.auth.ForgotPasswordActivity;
import com.upnvj.lahan.auth.LoginActivity;
import com.upnvj.lahan.homepage.product.add.AddProdukActivity;
import com.upnvj.lahan.homepage.tab.SectionsPagerAdapter;

public class ProdukActivity extends AppCompatActivity {
    ExtendedFloatingActionButton extendedfloatbutton;

    private final int[] TAB_TITLES_PRODUK = new int[]{
            R.string.tab_produk_text_1,
            R.string.tab_produk_text_2,
            R.string.tab_produk_text_3
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_produk);
        extendedfloatbutton = findViewById(R.id.fab);
        ActionBar actionBar = getSupportActionBar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Produk");

        ProdukSectionsPager produkSectionsPager = new ProdukSectionsPager(this);
        ViewPager2 viewPager = findViewById(R.id.produk_view_pager);
        viewPager.setAdapter(produkSectionsPager);
        TabLayout tabs = findViewById(R.id.produk_tabs);
        new TabLayoutMediator(tabs, viewPager,
                (tab, position) -> tab.setText(getResources().getString(TAB_TITLES_PRODUK[position]))
        ).attach();
        extendedfloatbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ProdukActivity.this, AddProdukActivity.class));
            }
        });
    }
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public void onBackPressed() {
        super.onBackPressed();
    }
}